---
title: "About"
output:
  workflowr::wflow_html:
    toc: false
editor_options:
  chunk_output_type: console
---

In this project, we studied the pattern of genetic variation and its association with environments for the wild barley population in the Southern Levant.

We focus on four objectives:  

* (1) describe the population structure of wild barley from the southern Levant and place it in the context of a worldwide sample (see Milner et al. 2019; https://www.nature.com/articles/s41588-018-0266-x)  
* (2) examine geographic patterns of gene flow in the southern Levant  
* (3) characterize the relative contributions of environmental gradients and space to genomic variation and population structure  
* (4) identify putative adaptive loci  
 

