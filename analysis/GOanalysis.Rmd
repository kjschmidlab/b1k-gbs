---
title: "Gene Ontology Enrichment Analysis"
author: "Che-Wei Chang"
date: "2021-10-25"
output: workflowr::wflow_html
editor_options:
  chunk_output_type: console
---

# Introduction
To investigate biological functions related to putatively adaptive loci, we conducted gene ontology (GO) enrichment analysis with gene annotations of the barley 'Morex v2' genome. We searched over-represented GO terms for genes within 500 bp adjacent intervals of candidate SNPs detected by the genome scans. This analysis was done using the R package `SNP2GO` [(Szkiba et al. 2014)](https://doi.org/10.1534/genetics.113.160341) with a significant level of FDR < 0.05.

# Searching genes within 500 bp intervals

We first looked for genes within 500 bp intervals adjacent to significant SNPs identified by genome scans. To do this, we used a customized function `search_gene_anno`.

## Reading required data
```{r, message=FALSE}
rm(list = ls())
source("./code/Rfunctions.R")

library(SNP2GO)
library(vcfR)
library(vegan)
library(qvalue)
library(robust)
library(IRanges)
library(GenomicRanges)
library(RCurl)
# to save computational time, we load the results of genome scans done previously
## load RDA results
rda.simple <- readRDS("./data/2020-04-10-simple_rda_191B1K+53KS_12envs_27147SNPs_maf0.05.RDS")
rda.ancescoff <- readRDS("./data/2020-04-10-partial_rda_ALStructure_K4_ancestry_coeff_191B1K+53KS_12envs_27147SNPs_maf0.05.RDS")

## load LFMM results
lfmm.p <- read.delim("./data/2020-04-18-191B1K+53KS_LFMM_K4_27147SNPs_maf0.05_12envs_adj_pvalues.tsv")
lfmm.q <- read.delim("./data/2020-04-18-191B1K+53KS_LFMM_K4_27147SNPs_maf0.05_12envs_adj_qvalues.tsv")

## load BAYPASS results
XtX <- read.table("./data/baypass_output_191B1K+53KS_in4pops_summary_pi_xtx.out", stringsAsFactors = F, header = T)
POD <- read.table("./data/wildbarley_POD_summary_pi_xtx.out", header = T)
pod.thres <- quantile(POD$M_XtX, 0.995)

# load gene annotations
gen.anno <- read.delim("./data/Barley_Morex_V2_gene_annotation_PGSB.all.descriptions.txt", header = T, stringsAsFactors = F)

# load VCF
geno <- read.vcfR("./data/GBS_244accessions_27147SNP_Beagle5Imp.vcf.gz", verbose = FALSE)
all.snp <- as.data.frame(geno@fix[,1:3])
CHR <- paste0("chr",as.numeric(geno@fix[,"CHROM"]), "H")
POS <- as.numeric(geno@fix[,"POS"])

# check the neighbour genes of all SNPs
pseudo.sig <- data.frame(chr = CHR, pos = POS)

gene.data <- data.frame(gene = gen.anno$gene, chr = gen.anno$agp_chr, start = gen.anno$start, end = gen.anno$end)
```

## Identification of genes nearby significant SNPs

### Simple RDA and partial RDA

```{r}

k = 4
qcut <- 0.05 # FDR threshold

rda.simple.pq <- rdadapt(rda.simple, K = k)
rda.ancescoff.pq <- rdadapt(rda.ancescoff, K = k)

# search genes within a 500 bp interval 

## Simple RDA
gene.data <- data.frame(gene = gen.anno$gene, chr = gen.anno$agp_chr, start = gen.anno$start, end = gen.anno$end)
rda.simple.sig <- data.frame(chr = CHR[rda.simple.pq$q.values < qcut], pos = POS[rda.simple.pq$q.values < qcut])
rda.simple.gene <- 
 gen.anno[gen.anno$gene %in% unique(unname(unlist(sapply(search_gene_anno(gene.pos = gene.data,snp.pos = rda.simple.sig,search.bp = 500), 
                                                        function(x){strsplit(x, split = ";")})))),]

## Partial RDA
rda.ancescoeff.sig <- data.frame(chr = CHR[rda.ancescoff.pq$q.values < qcut], pos = POS[rda.ancescoff.pq$q.values < qcut])

rda.ancescoeff.gene <- 
  gen.anno[gen.anno$gene %in% unique(unname(unlist(sapply(search_gene_anno(gene.pos = gene.data,snp.pos = rda.ancescoeff.sig, search.bp = 500), 
                                                          function(x){strsplit(x, split = ";")})))),]

str(rda.simple.gene)
str(rda.ancescoeff.gene)
```

### BAYPASS

```{r}
## BAYPASS
baypass.sig <- data.frame(chr = CHR[XtX$M_XtX > pod.thres], pos = POS[XtX$M_XtX > pod.thres])
baypass.gene <- 
  gen.anno[gen.anno$gene %in% unique(unname(unlist(sapply(search_gene_anno(gene.pos = gene.data,snp.pos = baypass.sig, search.bp = 500), 
                                                          function(x){strsplit(x, split = ";")})))),]

str(baypass.gene)
```

### LFMM

```{r}
## LFMM
lfmm.sig.tag <- apply(lfmm.q, 1, function(x){any(x < qcut)})
lfmm.sig <- data.frame(chr = CHR[lfmm.sig.tag], pos = POS[lfmm.sig.tag])

lfmm.gene <- 
  gen.anno[gen.anno$gene %in% unique(unname(unlist(sapply(search_gene_anno(gene.pos = gene.data,snp.pos = lfmm.sig, search.bp = 500), 
                                                          function(x){strsplit(x, split = ";")})))),]

str(lfmm.gene)
```

### Genes identified by multiple approaches

```{r}
# intersect of simple RDA and partial RDA
nrow(rda.simple.gene[rda.simple.gene$gene %in% rda.ancescoeff.gene$gene,])
# intersect of simple RDA and Baypass
nrow(rda.simple.gene[rda.simple.gene$gene %in% baypass.gene$gene,])
# intersect of simple RDA and LFMM
nrow(rda.simple.gene[rda.simple.gene$gene %in% lfmm.gene$gene,])
# intersect of partial RDA and Baypass
nrow(rda.ancescoeff.gene[rda.ancescoeff.gene$gene %in% baypass.gene$gene,])
# intersect of partial RDA and LFMM
nrow(rda.ancescoeff.gene[rda.ancescoeff.gene$gene %in% lfmm.gene$gene,])
# intersect of Baypass and LFMM
nrow(baypass.gene[baypass.gene$gene %in% lfmm.gene$gene,])

# intersect of all methods
overlap.gene <- rda.ancescoeff.gene[rda.ancescoeff.gene$gene %in% lfmm.gene$gene & rda.ancescoeff.gene$gene %in% baypass.gene$gene & rda.ancescoeff.gene$gene %in% rda.simple.gene$gene,]

overlap.gene
```

### Venn diagram

```{r}
# make Venn diagram
uniq.gene <- unique(c(as.character(rda.simple.gene$gene),    
                      as.character(rda.ancescoeff.gene$gene), 
                      as.character(baypass.gene$gene), as.character(lfmm.gene$gene)))


venn.input <- list("Simple RDA" = which(uniq.gene %in% as.character(rda.simple.gene$gene)), 
                   "Partial RDA" = which(uniq.gene %in% as.character(rda.ancescoeff.gene$gene)),
                   "LFMM" = which(uniq.gene %in% as.character(lfmm.gene$gene)), 
                   "BAYPASS" = which(uniq.gene %in% as.character(baypass.gene$gene))
)
library(VennDiagram)
venn <- 
  venn.diagram(venn.input,fill = "white"
               , cex = 1.5,cat.fontface = 4,lty =2,  imagetype = "png", 
               filename = NULL,
               cat.pos = c(0), resolution = 400, cat.cex = 2
  )


grid.draw(venn)
```

# GO term enrichment analysis

## Data preparation

```{r}
all.snp[,2] <- as.numeric(as.character(all.snp[,2]))
all.snp[,1] <- paste("chr",all.snp[,1], "H", sep = "") # make the coding of chromosome is identical to the `gff3` file
all.snp.G<- GRanges(seqnames=all.snp[,1],ranges=IRanges(all.snp[,2],all.snp[,2]))
 
# select candidates according to FDR
cand.rda.simple <- all.snp.G[which(rda.simple.pq[,2] < qcut)]
notcand.rda.simple <- all.snp.G[which(!rda.simple.pq[,2] < qcut)]

cand.rda.ancestry <- all.snp.G[which(rda.ancescoff.pq[,2] < qcut)]
notcand.rda.ancestry <- all.snp.G[which(!rda.ancescoff.pq[,2] < qcut)]


# 'snp2go' only search the lines of 'gene' for GO terms in the gff file
# but in our gff3 file the GO term is described in the lines of 'mRNA'
# so, I have to modify the our gff file to add GO terms to the 'gene'
gff_file <- "./data/Barley_Morex_V2_gene_annotation_PGSB.all.gff3"
gff <- readLines(gff_file)
gene <- gff[grepl(pattern = "\tgene\t", gff, perl = T)] #
mRNA <- gff[grepl(pattern = "\tmRNA\t", gff, perl = T)] #

new.gene <- paste(gene, sapply(strsplit(mRNA, split = ";"), function(x){paste(x[-1], collapse = ";")}), sep = ";")

gff[grepl(pattern = "\tgene\t", gff, perl = T)] <- new.gene

fileout<-file("./output/Barley_Morex_V2_gene_annotation_PGSB_append_GO.all.gff")
writeLines(gff, fileout)
close(fileout)
```

## Run `SNP2GO`

```{r eval=FALSE, include=TRUE}
gff_file <- "./output/Barley_Morex_V2_gene_annotation_PGSB_append_GO.all.gff"
ext = 500 # setting the searching range as 500 bp

GO.simpleRDA <- snp2go(gff=gff_file,
                       candidateSNPs=cand.rda.simple,
                       noncandidateSNPs=notcand.rda.simple, 
                       extension = ext,FDR = 0.05, runs = 10000)



GO.partialRDA <- snp2go(gff=gff_file,
                                candidateSNPs=cand.rda.ancestry,
                                noncandidateSNPs=notcand.rda.ancestry, 
                                extension = ext, FDR = 0.05, runs = 10000) # use 'FDR = 0.05' to keep only the significant results


# GO enrichment using LFMM result
qcut <- 0.05
ext = 500

for(i in 1:ncol(lfmm.q)){
  if(length(which(lfmm.q[i] < qcut) == 0)){next()} # skip if there is no candidate
  cand.lfmm <- all.snp.G[which(lfmm.q[i] < qcut)] # select candidate
  notcand.lfmm <- all.snp.G[which(!lfmm.q[i] < qcut)]
  
  env <- colnames(lfmm.q)[i]
  GO.lfmm <- snp2go(gff=gff_file,
                    candidateSNPs=cand.lfmm,
                    noncandidateSNPs=notcand.lfmm, 
                    extension = ext,FDR = 0.05, runs = 10000)
  
  saveRDS(GO.lfmm, paste("./output/", env,"-LFMM_FDR",qcut,"_SNP2GO_",ext,"bp_GOenrichFDR0.05.RDS", sep = ""))
}

# GO enrivhment using XtX result
ext = 500
cand.xtx <- all.snp.G[which(XtX$M_XtX > pod.thres)] # select candidate
notcand.xtx <- all.snp.G[which(!XtX$M_XtX > pod.thres)]

GO.xtx <- snp2go(gff=gff_file,
                 candidateSNPs=cand.xtx,
                 noncandidateSNPs=notcand.xtx, 
                 extension = ext,FDR = 0.05, runs = 10000)
```


## Check the number of enriched GO terms


```{r eval=TRUE, echo=FALSE}
# load results
GO.simpleRDA <- readRDS("./output/simple_RDA_FDR0.05_4axis_SNP2GO_500bp_FDR0.05.RDS")
GO.partialRDA <- readRDS("./output/partial_RDA_ancescoeff_FDR0.05_4axis_SNP2GO_500bp_FDR0.05.RDS")
GO.xtx <- readRDS("./output/XtX0.995_GOenrich_FDR0.05_500bp.RDS")
```
We identified no enriched GO term based on the SNPs detected by LFMM.

```{r, eval=TRUE, echo=TRUE}
sapply(list.files("./output/",pattern = "*LFMM_FDR*", full.names = TRUE), function(x){
  nrow(readRDS(x)$enriched)
}) # no enriched GO term for LFMM results
```

Ten and two GO terms enriched based on the SNPs detected by `XtX` and the simple RDA, respectively. However, no GO term was enriched based on the SNPs detected by the partial RDA.

```{r}
nrow(GO.xtx$enriched) # 10 enriched GO terms
nrow(GO.simpleRDA$enriched) # 2 enriched GO terms
nrow(GO.partialRDA$enriched) # no enriched GO term
```

