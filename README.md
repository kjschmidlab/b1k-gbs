# Barley 1K GBS Project

This is a repo for our **Barley 1K GBS** project. 

The preprint is available on bioRxiv (https://doi.org/10.1101/2021.09.15.460445).   
The peer-reviewed paper is on Heredity (https://www.nature.com/articles/s41437-021-00494-x).  

To view the website, please go to https://kjschmidlab.gitlab.io/b1k-gbs/

The website of this project is built by using [workflowr]: https://github.com/jdblischak/workflowr
